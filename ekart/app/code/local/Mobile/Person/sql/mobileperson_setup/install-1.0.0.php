<?php
$this->addAttribute('customer', 'mobile', array(
    'type'      => 'varchar',
    'label'     => 'Mobile Number',
    'input'     => 'text',
    'position'  => 120,
    'required'  => true,//or false
    'is_system' => 0,
));
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'mobile');
$attribute->setData('used_in_forms', array(
    'adminhtml_customer',
    'customer_account_create',
    'customer_account_edit',

));
$attribute->setData('is_user_defined', 0);
$attribute->save();